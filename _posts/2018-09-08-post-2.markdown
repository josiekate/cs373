---
layout: post
title:  "\\o/"
date:   2018-09-08
categories: blog post
---
**What did you do this past week?**  
This past week in SWE we started dissecting Python code, and went over the tools we’ll be using throughout the class. We also got our first project, and went over it in detail. Other classes are now giving homework too, and a few assignments have been due. Overall, things are slowly ramping up.

**What’s in your way?**  
I’m still waiting to see if my class load will be manageable, or if I should drop one of my fun classes. I only need a couple more classes to graduate, but I’ve realized that there are lots of things I still want to learn while I’m in school full-time. I don’t have a math class this semester, but I am taking Algo (sadly, I’m a year late and everyone I know has already taken it) and two mostly-freshman classes, Chem 1 and Econ 1. I’m a little far along to be weeded out, and I’m hoping the work will be interesting rather than tedious, but I still want to be able to devote most of my time to SWE. I’m also taking an online class about comics, but I’m determined to stay in that one no matter what.

**What will you do next week?**  
I’m hoping to finish Collatz fairly quickly, and also make a dent in my other homework. I don’t spend a lot of time sitting in classes, so my challenge this semester is to spend my free time judiciously. I’ll be traveling next weekend, so I want to get all my work out of the way by Friday.

**What's your experience of the class?**  
So far the class is enjoyable. I’m pretty familiar with most of the tools, and I’m excited to actually learn Python in a class setting (rather than stumbling through it as part of an internship). I’m really excited to get started on the group projects, but I’m also fine with using Collatz to get back into the swing of programming assignments. I always take a little bit to get used to new tools and their unique commands, and I’d rather not do that while a group is depending on me. 

**What’s your pick-of-the-week?**  
[Candy box !](https://candybox2.github.io/candybox/) Per its own site description, it’s “a role-playing browser video game featuring ASCII art.” I liked A Dark Room, but this is a lot longer, if only because I keep having to wait to get more candies. I’m not an avid gamer, so I like that it’s simple and doesn’t require a lot of skill. It’s pretty self-aware, and some levels expect you to edit the game’s JavaScript in order to pass. Also, who doesn’t love candy!

Good luck on Collatz!  
JosieKate  
<img src="https://josiekate.gitlab.io/cs373/assets/IMG_1064.JPG" alt="even_River_said_I_look_awkward" width="300"/>
