---
layout: post
title:  "Another busy week"
date:   2018-10-14
categories: blog post
---
**What did you do this past week?**  
In SWE, we continued learning Python with a focus on functions. I decided to get more serious about taking notes, because they help keep me focused in class and are easier for me to review than Downing’s GitHub notes are. On Wednesday I dropped a class, because I realized we get six Q-drops (as opposed to one like I thought… we get a “one time exception” that I guess is like a better Q-drop), so hopefully my schedule will free up a little now. I missed class on Friday, because I was in Seattle to have a final-round interview with Microsoft. I’m not sure how I did (all the interviewers were really nice, so it was hard to tell what kind of feedback they’ll end up giving), but it was nice to get a chance to explore Seattle, because this is where I’ve been concentrating my job search. 

**What’s in your way?**  
Two time zones, a 4-hour flight, and not enough sleep… I didn’t know when I requested these flights that a) the return flight time would be in PT, and b) I would have two tests on Monday. I haven’t been prioritizing studying as I should, but it’s hard to regret the long weekend in Seattle. I think my experience here will be helpful when it comes time to choose a job, and isn’t that what college is supposed to have been preparing me for? That’s what I’ve been telling myself, anyway. I’ll be studying on the flight, so wish me luck!

**What will you do next week?**  
Starting tomorrow I’ll take my two tests - chemistry and SWE. I’ll probably recuperate the rest of the week while I do the rest of my homework. I’m supposed to do backend work for my group’s SWE project, and because most of my week won’t have a lot going on, I’ll probably work on that. 

**What was your experience of learning the basics of Python?**  
I thought it was interesting - some of Python’s specifics seemed really useful and have clear use cases (like lambdas), and some were more confusing to me (I want there to be some way to reset an iter). As stated previously, I definitely got less out of the lectures when I didn’t take notes. I think the best way to learn these things would be to actually code solutions to problems using them, but I didn’t usually seek those out. Coding the functions themselves was somewhat helpful, but not something I see myself doing much outside of school.

**What’s your tip-of-the-week?**  
I haven’t signed an offer yet, but I’m done interviewing and job hunting. This is by no means an exhaustive or authoritative list, but there are some things I wish I had done differently (and some I think I did pretty well), so here they are:  
* Decide on a timeline (when to put more effort into applying, interview prep, negotiating, etc.): this is especially relevant if you have a returning offer. I didn’t make a strong commitment to getting or not getting competing offers, so I spent an unnecessary amount of time (and stress) waffling back and forth. If I had it to do over, I would do some research on what I could expect to gain from salary negotiations and what it would take to be in a good position to negotiate (which companies would give me high enough offers to compete), then decide whether the potential extra money was worth the extra stress.   
* Figure out what’s important to you: for me, it was 1. being near my partner in a place we could both get good jobs, 2. maximizing money-saving opportunities (high salary, low cost of living), 3. being somewhere I enjoy being (company and city). Other things might be more or less important to you - maybe you care a lot about a particular team or product, or a specific location, or vacation time. Prioritizing like this let me narrow my job search a lot, and helps me focus on the positive when I feel uncertain about my choices.  
* Comparison is the thief of joy: I think comparing salaries is important and especially useful for historically underpaid groups. However, if you’ve already decided on or committed to a position you’re excited about (and you’re not being criminally underpaid), try not to let a different great offer (another person’s or another company’s) make you feel bummed about your own. [Having a lot of choices can make us unhappy](http://www.swarthmore.edu/SocSci/bschwar1/Sci.Amer.pdf), and perfect jobs don’t exist.  
* Take easier / more flexible classes if possible: although this is often a useful strategy, it’s especially important if you really want to throw yourself into a job search. Missing class for things like career fairs and interviews (or vice versa) is stressful, and so is doing homework on the go (I say this from the Sea-Tac airport). I messed this one up and am suffering for it.   

Good luck on test 1!  
JosieKate  
<img src="https://josiekate.gitlab.io/cs373/assets/IMG_1064.JPG" alt="even_River_said_I_look_awkward" width="300"/>
