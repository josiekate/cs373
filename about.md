---
layout: page
title: About
permalink: /about/
---

I'm JosieKate and this is my SWE blog. I'm a senior (graduating in May 2019), and I've lived in Austin my whole life. When not coding, I can be found baking cupcakes and watching 60s Batman.

![Batmobile](https://josiekate.gitlab.io/cs373/assets/IMG_2596.jpg)